# Build A Snowman!

This game was developed as part of a project in the course TDT4240 Software Architecture. The objective of the game is to steer a snowman to catch the snowballs falling from the sky, while avoiding the falling fireballs. You can play by yourself, or have a friend join you to compete for the best score! 

<img src="assets/gameLogo.png" width="250" height="250"> 

## 💻 Getting started

1. 👇🏼 Clone the repository to Android Studios or a similar IDE. Make sure to have an updated version of Android SDK 34 installed.

Clone with:

https://oauth2:glpat-xb5moxz3vDAmZS6v4iYp@gitlab.stud.idi.ntnu.no/programvare-arkitektur/snowman-builder.git
or thorugh a zipped APK-file.

2. 🔧 Open the project in your IDE. To run the project, you will need gradle version 8.2.1 or higher. If you already have gradle installed, run the command `gradle --version`. If your gradle version is too old, sync with the project by going to `Android Studio -> File -> Sync Project with Gradle Files` or run the command `gradle clean build`.

3. 👾 Set up a virtual device through the Virtual Device (AVD) Manager configurations. For detailed instructions on how to set up an on-screen android emulator, please see this tutorial: https://developer.android.com/studio/run/emulator. If you have access to an Android device with Andoid 11 or higher, you can run the game on your device. For additional information on how to do this, please see https://developer.android.com/studio/run/device#connect. 

4. ⏳ The initial build of the project may take some time. Before running the application, make sure that the project build is successfully finished.

5. 🏃🏼 Run the app by choosing the correct AVD, and then press "Run" or run the command `gradle:run`. The virtual device may take some time the first time the app is run, as the AVD requires an initial setup.


> Not sure how the game works? Learn the rules by pressing the <img src="assets/buttons/menu/Tutorial.png" width="100" height="50"> button!

### Project Structure
The game was built using an MVC pattern, which is reflected in the project structure. Furthermore, the State and ECS patterns implemented are also visible throught the division of files.

Below is an overview of the current project structure. For readability purposes, files that are mandatory yet have not been altered since the initial commit have been excluded from the overview. These files mainly include various `gradle.build` and other gradle files. The individual images in `/assets` have also been excluded from the overview.
```
.
├── android
│   ├── AndroidManifest.xml
│   └── src
│       ├── google-services.json
│       └── com
│           └── buildasnowman
│               └── game
│                   ├── AndroidInterfaceClass.java
│                   └── AndroidLauncher.java
├── assets
│   ├── background/
│   ├── buttons/
│   ├── gameObjects/
│   ├── music/
│   └── gameLogo.png
├── core
│   └── src
│       └── com
│           └── buildasnowman
│               ├── backend
│               │   └── FirebaseInterface.java
│               ├── controllers
│               │   ├── EventHandler.java
│               │   ├── MultiplayerLobbyController.java
│               │   └── ViewManager.java
│               ├── game
│               │   ├── components
│               │   │   ├── CollisionComponent.java
│               │   │   ├── FallingComponent.java
│               │   │   ├── Mappers.java
│               │   │   ├── PositionComponent.java
│               │   │   ├── ScoreComponent.java
│               │   │   └── TextureComponent.java
│               │   ├── state
│               │   │   ├── PauseState.java
│               │   │   ├── PlayingState.java
│               │   │   ├── State.java
│               │   │   └── TimeUpState.java
│               │   ├── systems
│               │   │   ├── CollisionSystem.java
│               │   │   └── FallingSystem.java
│               │   ├── GameContext.java
│               │   ├── addtoECS.java
│               │   └── gameDraw.java
│               ├── models
│               │   ├── Player.java
│               │   └── Room.java
│               ├── util
│               │   ├── CreateButtonController.java
│               │   ├── CreateTextFieldController.java
│               │   ├── MusicManager.java
│               │   └── NavBarController.java
│               ├── views
│               │   ├── CreateMultiPlayer.java
│               │   ├── HighScoreView.java
│               │   ├── JoinMultiPlayer.java
│               │   ├── MenuView.java
│               │   ├── MultiplayerGameOverView.java
│               │   ├── SettingsView.java
│               │   ├── SinglePlayerGameOverView.java
│               │   ├── SnowmanGameView.java
│               │   ├── TutorialView.java
│               │   └── View.java
│               └── SnowmanGame.java
├── README.md
├── build.gradle
├── gradle.properties
└── settings.gradle
```

## Frameworks
The frontend is built in Android Studio with [LibGDX](https://libgdx.com/wiki/) and [AshleyECS](https://github.com/libgdx/ashley/wiki).  
The server is created using [Firebase Realtime Database](https://firebase.google.com/docs/database?hl=en).

## Sources

Sound "Game Music Loop 7", by XtremeFreddy:
https://pixabay.com/sound-effects/search/game%20music%20loop%207/ (29.03.2024)
