package com.buildasnowman.game;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.buildasnowman.SnowmanGame;

// Please note that on macOS your application needs to be started with the -XstartOnFirstThread JVM argument
public class DesktopLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setForegroundFPS(60);
		//Window size based on Gdx.graphics of android emulator, to maintain relative screen size
		config.setWindowedMode(1080/4, 2220/4);
		config.setTitle("Build A Snowman");
		new Lwjgl3Application( new SnowmanGame(new DesktopInterfaceClass()), config);
	}
}
