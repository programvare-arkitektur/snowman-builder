package com.buildasnowman.game;

import com.buildasnowman.backend.FirebaseInterface;
import com.buildasnowman.models.Player;

import java.util.List;

public class DesktopInterfaceClass implements FirebaseInterface {
    @Override
    public void createPlayer() {

    }

    @Override
    public Player getPlayer() {
        return null;
    }

   @Override
    public void SetOnValueChangedListener() {

    }

    @Override
    public void SetPlayerScoreInDb(String target, int value) {

    }

    @Override
    public void SetPlayerNameInDb(String target, String value) {

    }


    @Override
    public void SetRoomValueInDb(String target, String value) {

    }

    @Override
    public void ConnectToRoom(String roomID) {

    }

    @Override
    public void usersInRoom() {

    }

    @Override
    public void createRoom() {

    }

    @Override
    public String getRID() {
        return null;
    }

    @Override
    public String getPlayerName() {
        return null;
    }

    @Override
    public boolean isRoomReady() {
        return false;
    }

    @Override
    public boolean isMultiPlayer() {
        return false;
    }

    @Override
    public boolean RoomAlreadyExists() {
        return false;
    }

    @Override
    public int getMPResult() {
        return 0;
    }

    @Override
    public int getMPPlayerScore() {
        return 0;
    }

    @Override
    public int getOpponentScore() {
        return 0;
    }

    @Override
    public String getOpponentName() {
        return null;
    }

    @Override
    public List<Integer> retrieveHighScores() {
        return null;
    }

}
