package com.buildasnowman.game;

import android.util.Log;

import androidx.annotation.NonNull;

import com.buildasnowman.backend.FirebaseInterface;
import com.buildasnowman.models.Player;
import com.buildasnowman.models.Room;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

public class AndroidInterfaceClass implements FirebaseInterface {

    public FirebaseDatabase database;
    public DatabaseReference roomsRef;
    public DatabaseReference highScoresRef;
    public DatabaseReference playersRef;
    public Player player;
    public Room room;
    public String UID;
    public String RID;
    public String playerName;
    public boolean multiplayer;
    public int score;
    private boolean isHost;

    List<Player> scoresList = new ArrayList<>();


    public AndroidInterfaceClass() {
        // Write a message to the database
        database = FirebaseDatabase.getInstance("https://snowman-builder-default-rtdb.europe-west1.firebasedatabase.app/");
        playersRef = database.getReference("players");
        roomsRef = database.getReference("rooms");
        highScoresRef = database.getReference("highscores");
    }

    @Override
    public void createPlayer() {
        UID =  CreateUniquePlayerId();
        this.player = new Player(UID);
        playersRef.child(UID).setValue(player)

                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("Player created in db");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Error", e.toString());
                    }
                });
        this.playerName = "Olaf";
        this.player.setPlayerName(playerName);
        playersRef.child(UID).child("playerName").setValue(playerName);
        addToHighScore();
    }

    @Override
    public Player getPlayer() {
        return null;
    }

    @Override
    public boolean isMultiPlayer() {
        return multiplayer;
    }

    private void setMultiPlayer(boolean isMultiPlayer) {
        this.multiplayer = isMultiPlayer;
    }

    @Override
    public void SetOnValueChangedListener() {

    }

    public String CreateUniquePlayerId (){
        return UUID.randomUUID().toString();
    }

    public void addToHighScore(){
        score = this.player.getHighScore();
        highScoresRef.child(UID).setValue(score)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("highscore added in db");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Error", e.toString());
                    }
                });
        updatePlayerScore(this.player, true);
        retrieveHighScores();
    }

    public List<Player> retrieveHighScores() {
        List<Player> scoresListTemporary = new ArrayList<>();
        playersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Player score = snapshot.getValue(Player.class); // Get the score
                    scoresListTemporary.add(score); // Add the score to the list'
                }
                Collections.sort(scoresListTemporary, (o1, o2) -> o2.getHighScore() - o1.getHighScore());
                scoresList = scoresListTemporary;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle errors
                Log.e("Firebase", "Error retrieving high scores: " + databaseError.getMessage());
            }
        });
        return scoresList;
    }

    public void createRoom() {
        RID = createRoomID();
        this.room = new Room(RID, player, new Player(""));
        roomsRef.child(RID).setValue(room)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("Room created in db");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("Error: room not created", e.toString());
                    }
                });
        //add listener to update player score in room
        updatePlayerScore(player, false);
        //add listener to wait for guest to join
        waitForGuest(room);
    }

    public String getRID() { return this.RID;}

    @Override
    public String getPlayerName(){
        return this.player.getPlayerName();
    }

    @Override
    public void SetPlayerNameInDb(String target, String value) {
        player.setPlayerName(value);
        DatabaseReference reference = playersRef.child(player.getUID());
        reference.child(target).setValue(value);
    }

    @Override
    public void SetRoomValueInDb(String target, String value) {

    }

    @Override
    public void SetPlayerScoreInDb(String target, int value) {
        DatabaseReference reference = playersRef.child(player.getUID());
        player.setHighScore(value);
        int score = player.getHighScore();
        reference.child(target).setValue(score);
    }

    @Override
    public boolean RoomAlreadyExists() {
        return room != null;
    }

    @Override
    public void ConnectToRoom(String roomID) {
        //initiates local room with oneself as guest, since joining existing room
        this.room = new Room("", new Player(""), player);

        roomsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.hasChild(roomID)) {
                    DatabaseReference reference = roomsRef.child(roomID);
                    RID = roomID;
                    reference.child("guestPlayer").child("uid").setValue(UID);

                    //sets the rest of the room to match the server - add roomID and host player
                    String host = (String) snapshot.child(roomID).child("hostPlayer").child("uid").getValue();
                    room.setRoomID(roomID);
                    room.setHostPlayer(new Player(host));
                    room.setRoomReady(true);

                    //add listener to update score in room
                    updatePlayerScore(player, false);
                } else {
                    room = null; //remove local room if ID does not exist
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Error", error.toString());
            }
        });
    }

    /**
     * Adds a listener to the score of a player in "Players" in database,
     * and updates correlating player objects in "High scores" and "Rooms" with current score.
     * @param p player to listen to
     * @param ignoreRoom do not account for room if player is not in multiplayer
     */
    public void updatePlayerScore(Player p, boolean ignoreRoom) {
        isHost = false;
        //checks if player is host
        if(!ignoreRoom) {
            isHost = Objects.equals(p.getUID(), room.hostPlayer.getUID());
        }
        ValueEventListener scoreListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                assert snapshot.hasChild("highScore");
                Long score = snapshot.child("highScore").getValue(Long.class);

                if (score != null) {
                    //updates the correct player in high scores
                    highScoresRef.child(p.getUID()).setValue(score.intValue());
                    if(!ignoreRoom) {
                            //updates the correct player in room
                       if (isHost) {
                            room.hostPlayer.setHighScore(score.intValue());
                            roomsRef.child(room.roomID).child("hostPlayer").setValue(room.hostPlayer);
                        } else {
                            room.guestPlayer.setHighScore(score.intValue());
                            roomsRef.child(room.roomID).child("guestPlayer").setValue(room.guestPlayer);
                        }

                            //if player p is this player, update score
                            if (Objects.equals(p.getUID(), player.getUID())) {
                                player.setHighScore(score.intValue());
                            }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("Error adding listener: ", error.toString());
            }
        };

        //adds listener to player
        playersRef.child(p.getUID()).addValueEventListener(scoreListener);
    }

    @Override
    public int getMPPlayerScore(){
        int score = 0;

        //checks whether user is host or guest in room
        if (Objects.equals(player.getUID(), room.hostPlayer.getUID())) {
            score = room.hostPlayer.getHighScore();
        } else if (Objects.equals(player.getUID(), room.guestPlayer.getUID())) {
            score = room.guestPlayer.getHighScore();
        }

        return score;
    }

    @Override
    public int getOpponentScore() {
        int score = 0;

        //checks whether opponent is host or guest
        if (Objects.equals(player.getUID(), room.hostPlayer.getUID())) {
            score = room.guestPlayer.getHighScore();
        } else if (Objects.equals(player.getUID(), room.guestPlayer.getUID())) {
            score = room.hostPlayer.getHighScore();
        }

        return score;
    }

    @Override
    public String getOpponentName(){
        String name = "";

        //checks whether opponent is host or guest
        if (Objects.equals(player.getUID(), room.hostPlayer.getUID())) {
            name = room.guestPlayer.getPlayerName();
        } else if (Objects.equals(player.getUID(), room.guestPlayer.getUID())) {
            name = room.hostPlayer.getPlayerName();
        }

       return name;
    }

    /**
     * Fetches name from playersRef and updates roomsRef and local
     * @param p player to update
     */
    public void updatePlayerName(Player p) {

        playersRef.child(p.getUID()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String s = String.valueOf(snapshot.child("playerName").getValue());
                //updates the correct player's username in room
                if (Objects.equals(p.getUID(), room.hostPlayer.getUID())) {
                    room.hostPlayer.setPlayerName(s);
                } else if (Objects.equals(p.getUID(), room.guestPlayer.getUID())) {
                    room.guestPlayer.setPlayerName(s);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void updateRoomInfo() {
        //makes sure name info is updated in room
        updatePlayerName(room.hostPlayer);
        updatePlayerName(room.guestPlayer);
    }

    /**
     * Checks whether player is host or guest, and whether host or guest won the game.
     * @return int gameResult
     * 1: player won
     * 2: tied scores
     * 3: player lost
     */
    @Override
    public int getMPResult() {

        int gameResult;
        boolean isHost = Objects.equals(player.getUID(), room.hostPlayer.getUID());

        updateRoomInfo();

        if ((isHost && room.hostPlayer.getHighScore() > room.guestPlayer.getHighScore()) || (!isHost && room.guestPlayer.getHighScore() > room.hostPlayer.getHighScore())) {
            gameResult = 1;
        } else if (room.hostPlayer.getHighScore() == room.guestPlayer.getHighScore()) {
            gameResult = 2;
        } else {
            gameResult = 3;
        }

        return gameResult;
    }

    /**
     * Generates a random 5-digit code to use as room ID
     * @return roomID code
     */
    public String createRoomID(){
        Random random = new Random();
        int num = random.nextInt(99999);
        return String.format("%05d", num);
    }

    /**
     * Adds a listener to a server room which is triggered when guest ID is changed.
     * When triggered, guest ID is added to room model, and room status is set to ready
     * @param r ID of the room
     */
    public void waitForGuest(Room r) {
        ValueEventListener guestListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                assert snapshot.hasChild("uid");
                String uid = (String) snapshot.child("uid").getValue();

                if (!Objects.equals(uid, "")) {
                    room.setGuestPlayer(new Player(uid));
                    room.setRoomReady(true);
                    roomsRef.child(r.roomID).child("roomReady").setValue(true);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        //adds listener to room
        roomsRef.child(r.roomID).child("guestPlayer").addValueEventListener(guestListener);
    }

    /**
     * Checks if room exists, and if status of room is ready.
     * If room exists and both players are in, multiplayer status is set to true.
     * @return room status
     */
    public boolean isRoomReady() {
        boolean ready;

        if (room == null) {
            ready = false;
        } else {
            ready = room.isRoomReady();
            setMultiPlayer(ready);
        }

        return ready;
    }
}