package com.buildasnowman.game;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.buildasnowman.SnowmanGame;

/** This class is the entry point for the Android application */
public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		// This method is called when the application is started
		super.onCreate(savedInstanceState);
		// Create a new AndroidApplicationConfiguration object
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		initialize( new SnowmanGame(new AndroidInterfaceClass()), config);
	}
}
