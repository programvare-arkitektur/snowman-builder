package com.buildasnowman.backend;

import com.buildasnowman.models.Player;

import java.util.List;

public interface FirebaseInterface {

    void createPlayer();

    Player getPlayer();

    void SetOnValueChangedListener();

    void SetPlayerScoreInDb(String target, int value);

    void SetPlayerNameInDb(String target, String value);

    void SetRoomValueInDb(String target, String value);

    void ConnectToRoom(String roomID);

    void createRoom();

    String getRID();

    String getPlayerName();

    boolean isRoomReady();

    boolean isMultiPlayer();

    boolean RoomAlreadyExists();

    int getMPResult();

    int getMPPlayerScore();

    int getOpponentScore();

    String getOpponentName();

    List<Player> retrieveHighScores();
}


