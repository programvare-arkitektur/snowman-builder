package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.util.CreateTextFieldController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;

public class CreateMultiPlayer extends View {
    private Stage stage;
    private Texture backgroundImg;
    private ImageButton menuButton, createGameIButton;
    String gameCode;
    private TextField gameCodeField, friendsField;
    CreateButtonController createImgBtn;
    CreateTextFieldController createText;
    NavBarController navBar;

    public CreateMultiPlayer(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);
        createText = new CreateTextFieldController();
        navBar = new NavBarController();

        //initiate a game room in server
        MPController.createRoom();
        gameCode = MPController.getRoomCode();
        backgroundImg = new Texture("background/Background.png");

        //create game button
        createImgBtn = new CreateButtonController();
        createGameIButton = createImgBtn.createImageButton("buttons/menu/JoinGame.png");
        // Create the menu button (back button)
        menuButton = createImgBtn.createImageButton("buttons/BackButton.png");

        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController);
        eventHandler.listener(menuButton, "menu");
        eventHandler.listener(createGameIButton, "joinGame");


       drawUI();
    }

    private String getFriendsJoined() {
        String msg = "waiting for friend to join..";
        boolean roomFull = MPController.checkRoomStatus(); //checks if friend has joined
        if (roomFull) {
            msg = "friend has joined!";
        }
        return msg;
    }

    /**
     * Handles button events. create game now send into single game, temporarily.
     * @param name from button listener */
    @Override
    protected void handleInput(String name) {
        if (name.equals("back")) {
            Gdx.input.setOnscreenKeyboardVisible(false); // Closes the keyboard if left opened
            vm.set(new MenuView(vm, musicManager, MPController));
        } else if (name.equals("createGame")) {
            boolean roomReady = MPController.checkRoomStatus(); //checks if the room has registered two users
            if (roomReady) {
                vm.set(new SnowmanGameView(vm, musicManager, MPController));
            }
        }
    }

    @Override
    public void update(float dt) {
        friendsField.setMessageText(getFriendsJoined());
    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();
        sb.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), backgroundImg.getHeight() * ((float) Gdx.graphics.getWidth() / backgroundImg.getWidth()));
        sb.end();
        stage.draw();
        stage.act();
    }

    @Override
    public void dispose() {
        stage.dispose();
        backgroundImg.dispose();
    }

    @Override
    public void drawUI() {
        stage = new Stage();

        stage.addActor(navBar.createNavBar(menuButton));
        gameCodeField = createText.createTextField("", 4f, null);
        gameCodeField.setMessageText("PIN: " + gameCode);
        gameCodeField.setDisabled(true);

        /*
        Label for friend joining lobby
         */
        friendsField = createText.createTextField("", 4f, null);
        friendsField.setMessageText(getFriendsJoined());
        friendsField.setDisabled(true);


        //Table for create game button
        Table createGameTable = new Table();
        createGameTable.setFillParent(true);
        createGameTable.columnDefaults(0).width(Gdx.graphics.getWidth()/2f).height(Gdx.graphics.getHeight()/3f);
        createGameTable.add(gameCodeField);

        createGameTable.row();
        createGameTable.add(friendsField);

        createGameTable.row();
        createGameTable.add(createGameIButton);

        //copied from SettingsView:
        stage.addActor(createGameTable);

        Gdx.input.setInputProcessor(stage); // make the buttons clickable

    }
}
