package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.SnowmanGame;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.util.CreateTextFieldController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;

public class SettingsView extends View {
    private Stage stage;
    private NavBarController navBar;
    private CreateButtonController createImgBtn;
    private CreateTextFieldController createText;
    private Texture backgroundImg, textFieldBackground;
    private ImageButton menuButton, soundToggleButton;
    private TextureRegionDrawable textFieldBackgroundDrawable;
    private TextField nameField;
    String userName;
    private boolean soundOn;


    public SettingsView(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);
        navBar = new NavBarController();
        createImgBtn= new CreateButtonController();
        createText = new CreateTextFieldController();

        backgroundImg = new Texture("background/Background.png");
        // Set the sound to be on by default
        soundOn = musicManager.isBackgroundMusicPlaying();

        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController);
        menuButton = createImgBtn.createImageButton("buttons/BackButton.png");
        eventHandler.listener(menuButton, "menu");

        drawUI();
    }

    @Override
    public void drawUI(){

        // stage for the buttons
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        if (soundOn){
            soundToggleButton = createImgBtn.createImageButton("buttons/settings/soundOnHorizontal.png");
        }
        else{
            soundToggleButton = createImgBtn.createImageButton("buttons/settings/soundOffHorizontal.png");
        }

        // add listener to the sound button
        soundToggleButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                toggleSound();
            }
        });

        textFieldBackground = new Texture("background/TextfieldBackground.png");
        textFieldBackgroundDrawable = new TextureRegionDrawable(new TextureRegion(textFieldBackground));

        // gets playerName from database
        userName = SnowmanGame.FBI.getPlayerName();
        nameField = createText.createTextField(userName, 4.8f, textFieldBackgroundDrawable );
        nameField.setSize(textFieldBackground.getWidth(), textFieldBackground.getHeight());
        nameField.setAlignment(Align.center);
        nameField.setMessageText(userName);
        nameField.setMaxLength(10);

        Table table = new Table();
        table.setFillParent(true);
        table.columnDefaults(0).width(Gdx.graphics.getWidth()/1.5f).height(Gdx.graphics.getHeight()/3f);
        table.add(nameField).center();
        table.row();
        table.add(soundToggleButton).center();
        table.row();


        // add the table to the stage
        stage.addActor(table);
        stage.addActor(navBar.createNavBar(menuButton));

        nameField.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (c == '\n') {
                    // Update the userName with the text input
                    userName = textField.getText();
                    SnowmanGame.FBI.SetPlayerNameInDb("playerName", userName);
                }
            }
        });
    }

    private void toggleSound() {
        if (soundOn) {
            musicManager.stopBackgroundMusic();
            soundToggleButton.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("buttons/settings/soundOffHorizontal.png")));
        } else {
            musicManager.playBackgroundMusic();
            soundToggleButton.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(new Texture("buttons/settings/soundOnHorizontal.png")));
        }
        soundOn = !soundOn; // Toggle sound state
    }

    @Override
    protected void handleInput(String name){    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), backgroundImg.getHeight() * (Gdx.graphics.getWidth() / backgroundImg.getWidth()));
        sb.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        backgroundImg.dispose();
        stage.dispose();
    }

}
