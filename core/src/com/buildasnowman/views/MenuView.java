package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;

/**
 * A view for the main menu of the game
 * Contains buttons for single player, multiplayer, settings, tutorial and highscore
 */
public class MenuView extends View {
    private Stage stage;
    Texture backgroundImg;
    ImageButton playButtonImage, createMPButtonImage, joinMPButtonImage, settingsButtonImage, tutorialButtonImage, highscoreButtonImage, gameLogoImg;


    public MenuView(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);

        backgroundImg = new Texture("background/Background.png");

        CreateButtonController createImgBtn = new CreateButtonController();

        // Create the buttons
        gameLogoImg = createImgBtn.createImageButton("gameLogo.png");
        playButtonImage = createImgBtn.createImageButton("buttons/menu/SinglePlayer.png");
        createMPButtonImage = createImgBtn.createImageButton("buttons/menu/InviteFriends.png");
        joinMPButtonImage = createImgBtn.createImageButton("buttons/menu/JoinGame.png");
        settingsButtonImage = createImgBtn.createImageButton("buttons/menu/Settings.png");
        tutorialButtonImage = createImgBtn.createImageButton("buttons/menu/Tutorial.png");
        highscoreButtonImage = createImgBtn.createImageButton("buttons/menu/Leaderboard.png");

        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController);

        // Add listeners to the buttons
        eventHandler.listener(playButtonImage, "playButton");
        eventHandler.listener(createMPButtonImage, "createMPButton");
        eventHandler.listener(joinMPButtonImage, "joinMPButton");
        eventHandler.listener(settingsButtonImage, "settingsButton");
        eventHandler.listener(tutorialButtonImage, "tutorialButton");
        eventHandler.listener(highscoreButtonImage, "highscoreButton");

        drawUI();
    }

    @Override
    public void drawUI(){
        // Create table to store the logo
        Table logoTable = new Table();
        logoTable.setFillParent(true);
        logoTable.add(gameLogoImg).height(Gdx.graphics.getHeight() / 2.2f).top();
        logoTable.center().top();
        logoTable.padTop(Gdx.graphics.getHeight()/20f);

        // Create table to store the buttons
        Table table = new Table();
        table.setFillParent(true);
        table.columnDefaults(0).width(Gdx.graphics.getWidth()/2f).height(Gdx.graphics.getHeight()/7f);
        table.columnDefaults(1).width(Gdx.graphics.getWidth()/2f).height(Gdx.graphics.getHeight()/7f);
        table.add(playButtonImage);
        table.add(joinMPButtonImage);
        table.row();
        table.add(settingsButtonImage);
        table.add(createMPButtonImage);
        table.row();
        table.add(highscoreButtonImage);
        table.add(tutorialButtonImage);
        table.bottom();
        table.padBottom(Gdx.graphics.getHeight()/10f);

        stage = new Stage();
        stage.addActor(logoTable);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }
    @Override
    protected void handleInput(String name) {
        switch (name) {
            case "playButton":
                vm.set(new SnowmanGameView(vm, musicManager, MPController));
                break;
            case "settingsButton":
                vm.set(new SettingsView(vm, musicManager, MPController));
                break;
            case "createMPButton":
                vm.set(new CreateMultiPlayer(vm, musicManager, MPController));
                break;
            case "joinMPButton":
                vm.set(new JoinMultiPlayer(vm, musicManager, MPController));
                break;
            case "tutorialButton":
                vm.set(new TutorialView(vm, musicManager, MPController));
                break;
            case "highscoreButton":
                vm.set(new HighScoreView(vm, musicManager, MPController));
                break;
        }
    }

    @Override
    public void update(float dt) {
    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();
        sb.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), backgroundImg.getHeight() * ( (float) Gdx.graphics.getWidth() / backgroundImg.getWidth()));
        sb.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
        backgroundImg.dispose();
    }
}
