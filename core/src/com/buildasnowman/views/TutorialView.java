package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;

public class TutorialView extends View{
    private Stage stage;
    private NavBarController navBar;
    private Texture tutorialImg;

    private ImageButton menuButton;

    public TutorialView(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);
        navBar = new NavBarController();

        // set the background to be the tutorial image
        tutorialImg = new Texture("background/Tutorial.png");

        // create the buttons for this view
        CreateButtonController createImgBtn = new CreateButtonController();
        menuButton = createImgBtn.createImageButton("buttons/BackButton.png");

        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController);
        eventHandler.listener(menuButton, "menu");

        drawUI();
    }

    @Override
    protected void handleInput(String name) {
    }
    @Override
    public void drawUI() {

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.addActor(navBar.createNavBar(menuButton));
    }
    @Override
    public void update(float dt) {
    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();
        sb.draw(tutorialImg, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        sb.end();
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
