package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.SnowmanGame;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;
import com.buildasnowman.game.GameContext;
import com.buildasnowman.game.gameDraw;
import com.buildasnowman.game.state.PlayingState;
import com.buildasnowman.game.state.TimeUpState;

public class SnowmanGameView extends View {
    private GameContext game;
    private NavBarController navBar;
    private CreateButtonController createImgBtn;
    Stage stage;
    ImageButton menuImageButton, pauseImageButton;
    gameDraw draw;

    public SnowmanGameView(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);
        createImgBtn = new CreateButtonController();
        navBar = new NavBarController();

        // Set the initial state of game
        game = GameContext.getInstance();
        game.setState(new PlayingState(game));
        draw =  gameDraw.getInstance(game);

        // buttons
        menuImageButton = createImgBtn.createImageButton("buttons/BackButton.png");
        pauseImageButton = createImgBtn.createImageButton("buttons/PauseButton.png");
        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController, game, draw);
        eventHandler.listener(menuImageButton, "GameMenu");
        eventHandler.listener(pauseImageButton, "pause");

        drawUI();
    }

    @Override
    public void drawUI() {

        stage = new Stage();
        stage.addActor(navBar.createNavBar(menuImageButton, pauseImageButton));
        // make the buttons clickable
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    protected void handleInput(String name) {
    }

    @Override
    public void update(float dt) {
        if (game.getState() instanceof TimeUpState) {
            if (SnowmanGame.FBI.isMultiPlayer()) {
                vm.set(new MultiplayerGameOverView(vm, musicManager, MPController));
            } else {
                vm.set(new SinglePlayerGameOver(vm, musicManager, MPController, game.getScore()));
            }
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();
        draw.draw(sb);
        game.handleGame(Gdx.graphics.getDeltaTime(), sb);
        sb.end();
        stage.draw();
    }

    @Override
    public void dispose() {
        game.dispose();
        stage.dispose();
    }
}
