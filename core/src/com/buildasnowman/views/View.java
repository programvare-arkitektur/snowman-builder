package com.buildasnowman.views;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.controllers.ViewManager;

/**
 * A general View to manage game views
 */
public abstract class View {

    protected ViewManager vm;
    protected MusicManager musicManager;
    protected MultiplayerLobbyController MPController;

    public View(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        this.vm = viewManager;
        this.musicManager = musicManager;
        this.MPController = MPController;

    }

    protected abstract void handleInput(String name);

    public abstract void update(float dt);

    public abstract void render(SpriteBatch sb);

    public abstract void dispose();

    public abstract void drawUI();

}
