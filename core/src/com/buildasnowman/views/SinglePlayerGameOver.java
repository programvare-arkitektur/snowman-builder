package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;

public class SinglePlayerGameOver extends View{

    // Background
    private Texture backgroundImg;
    private Stage stage;
    private ImageButton menuButton;

    private String score;

    private NavBarController navBar;
    private CreateButtonController createImgBtn;


    public SinglePlayerGameOver(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController, int score) {
        super(viewManager, musicManager, MPController);
        this.score = String.valueOf(score);

        navBar = new NavBarController();
        // Set the background to be the game over image
        backgroundImg = new Texture("background/Background.png");

        // Create the buttons for this view
        createImgBtn = new CreateButtonController();
        menuButton = createImgBtn.createImageButton("buttons/BackButton.png");

        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController);
        eventHandler.listener(menuButton, "menu");

        drawUI();

    }

    @Override
    public void drawUI() {
        // Ensure stage input processor is set to handle UI events
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        // create a unique table for the menu button, to give it a different position
        stage.addActor(navBar.createNavBar(menuButton));

        // Create the leaderboard table and set its layout
        Table ScoreTable = new Table();
        ScoreTable.setFillParent(true);
        ScoreTable.center();
        stage.addActor(ScoreTable);

        // Create a label for the title of the highscore table
        Label ScoreLabel = new Label("Score:", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        ScoreLabel.setFontScale(5); // Increase the font size of the title
        ScoreTable.add(ScoreLabel).colspan(2).padBottom(30); // Add title label and span across 2 columns
        ScoreTable.row(); // Move to the next row
        Label ScoreAmount = new Label(score, new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        ScoreAmount.setFontScale(3);
        ScoreTable.add(ScoreAmount);

    }

    @Override
    protected void handleInput(String name) {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), backgroundImg.getHeight() * ((float) Gdx.graphics.getWidth() / backgroundImg.getWidth()));
        sb.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        backgroundImg.dispose();
        stage.dispose();
    }
}
