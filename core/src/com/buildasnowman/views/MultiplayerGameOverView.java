package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.util.CreateTextFieldController;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;

public class MultiplayerGameOverView extends View{

    // Background
    private Texture backgroundImg;
    private Stage stage;
    private ImageButton menuImageButton;
    CreateButtonController createImgBtn;

    CreateTextFieldController createText;
    NavBarController navBar;

    public MultiplayerGameOverView(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);

        backgroundImg = new Texture("background/Background.png");
        createImgBtn = new CreateButtonController();
        createText = new CreateTextFieldController();
        navBar = new NavBarController();

        System.out.println("In GameOver: ");

        drawUI();
    }



    @Override
    public void drawUI() {
        //Field announcing win, loss or tie
        TextField resultField = createText.createTextField("", 4f, null);
        resultField.setMessageText(createResultText(MPController.getResult()));

        //Fields summarizing scores
        TextField playerResultField = createText.createTextField("", 4f, null);
        playerResultField.setMessageText("You: " + MPController.getPlayerScore());

        TextField opponentResultField = createText.createTextField("", 4f, null);
        opponentResultField.setMessageText(MPController.getOpponentName() + ": " + MPController.getOpponentScore());

        //Table for result textFields
        Table resultTable = new Table();
        resultTable.setFillParent(true);
        resultTable.columnDefaults(0).width(Gdx.graphics.getWidth()/2f).height(Gdx.graphics.getHeight()/5f);
        resultTable.add(resultField);
        resultTable.row();
        resultTable.add(playerResultField);
        resultTable.row();
        resultTable.add(opponentResultField);

        //copied from SettingsView:
        stage = new Stage();
        stage.addActor(resultTable);

        // Create the menu button (back button)
        menuImageButton = createImgBtn.createImageButton("buttons/BackButton.png");
        menuImageButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                handleInput("menu");
            }
        });
        // Add the menu table to the stage
        stage.addActor(navBar.createNavBar(menuImageButton));
    }

    public String createResultText(int result) {
        String resultText = "";

        if (result == 1) {
            resultText = "You won!";
        } else if (result == 2) {
            resultText = "It's a tie!";
        } else if (result == 3) {
            resultText = "You lost!";
        }

        return resultText;
    }

    @Override
    protected void handleInput(String name) {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        sb.begin();
        sb.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), backgroundImg.getHeight() * ((float) Gdx.graphics.getWidth() / backgroundImg.getWidth()));
        sb.end();
        stage.draw();
        stage.act();
    }

    @Override
    public void dispose() {

    }
}
