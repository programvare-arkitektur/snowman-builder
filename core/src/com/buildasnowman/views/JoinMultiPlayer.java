package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.util.CreateTextFieldController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;

public class JoinMultiPlayer extends View {

    private Stage stage;

    private Texture backgroundImg;
    private ImageButton joinGameImageButton, menuButton;

    String gameCode;

    private TextField gameCodeField, gameCodeLabelField, errorField;

    CreateButtonController createImgBtn;
    CreateTextFieldController createText;
    NavBarController navBar;

    public JoinMultiPlayer(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);
        createText = new CreateTextFieldController();
        navBar = new NavBarController();
        backgroundImg = new Texture("background/Background.png");

        //create the buttons for this view
        createImgBtn = new CreateButtonController();
        menuButton = createImgBtn.createImageButton("buttons/BackButton.png");
        joinGameImageButton = createImgBtn.createImageButton("buttons/menu/JoinGame.png");

        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController);
        eventHandler.listener(menuButton, "menu");
        eventHandler.listener(joinGameImageButton, "joinGame");

        Texture textFieldBackground = new Texture("background/TextfieldBackground.png");
        TextureRegionDrawable textFieldBackgroundDrawable = new TextureRegionDrawable(new TextureRegion(textFieldBackground));

        //game code input field
        gameCode = "";
        gameCodeField = createText.createTextField(gameCode, 8f, textFieldBackgroundDrawable);
        gameCodeField.setSize(textFieldBackground.getWidth(), textFieldBackground.getHeight());
        gameCodeField.setAlignment(Align.center);

        //game code label text field
        gameCodeLabelField = createText.createTextField("", 5f, null);
        gameCodeLabelField.setMessageText("Enter game pin:");
        gameCodeLabelField.setDisabled(true);

        //error text field, initially invisible
        errorField = createText.createTextField("", 5f, null);
        errorField.setMessageText("Invalid code");
        errorField.setDisabled(true);
        errorField.setVisible(false);


        drawUI();
    }


    @Override
    public void drawUI() {


        //Table for join game button
        Table joinGameTable = new Table();
        joinGameTable.setFillParent(true);
        joinGameTable.columnDefaults(0).width(Gdx.graphics.getWidth()/2f).height(Gdx.graphics.getHeight()/4f);
        joinGameTable.add(gameCodeLabelField);

        joinGameTable.row();
        joinGameTable.add(gameCodeField);

        joinGameTable.row();
        joinGameTable.add(errorField);

        joinGameTable.row();
        joinGameTable.add(joinGameImageButton);

        stage = new Stage();
        stage.addActor(joinGameTable);
        // Add the menu table to the stage
        stage.addActor(navBar.createNavBar(menuButton));
        //initial focus on the game code text field and opens device keyboard
        stage.setKeyboardFocus(gameCodeField);
        gameCodeField.getOnscreenKeyboard().show(true);
        Gdx.input.setInputProcessor(stage); // make the buttons clickable

        /**
         * Listeners to the buttons. Copied from SettingsView*/

        gameCodeField.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (c == '\n') {
                    // Update the gamecode field with the text input and attempt to connect to game room
                    gameCode = textField.getText();
                    MPController.connectToRoom(gameCode);
                }
            }
        });
    }

    @Override
    protected void handleInput(String name) {
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {
        //copied from settingsView
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        sb.begin();
        sb.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), backgroundImg.getHeight() * ((float) Gdx.graphics.getWidth() / backgroundImg.getWidth()));
        sb.end();
        stage.draw();
        stage.act();
    }

    @Override
    public void dispose() {
        stage.dispose();
        backgroundImg.dispose();
    }
}
