package com.buildasnowman.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.SnowmanGame;
import com.buildasnowman.controllers.ViewManager;
import com.buildasnowman.models.Player;
import com.buildasnowman.util.CreateButtonController;
import com.buildasnowman.controllers.EventHandler;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.util.NavBarController;

import java.util.List;

public class HighScoreView extends View {

    private Texture backgroundImg;
    private Stage stage;
    private List<Player> highScoreList;
    private ImageButton menuButton;

    private final NavBarController navBar;
    private final CreateButtonController createImgBtn;



    public HighScoreView(ViewManager viewManager, MusicManager musicManager, MultiplayerLobbyController MPController) {
        super(viewManager, musicManager, MPController);
        navBar = new NavBarController();
        backgroundImg = new Texture("background/Background.png");

        // Create button for this view
        createImgBtn = new CreateButtonController();
        menuButton = createImgBtn.createImageButton("buttons/BackButton.png");

        EventHandler eventHandler = new EventHandler(vm, musicManager, MPController);
        eventHandler.listener(menuButton, "menu");

        highScoreList = SnowmanGame.FBI.retrieveHighScores();

        drawUI();
    }

    @Override
    public void drawUI() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        // Add the menu table to the stage
        stage.addActor(navBar.createNavBar(menuButton));

        // Create the leaderboard table and set its layout
        Table highScoreTable = new Table();
        highScoreTable.setFillParent(true);
        highScoreTable.center();
        stage.addActor(highScoreTable);

        // Create a label for the title of the highscore table
        Label highScoreLabel = new Label("Highscore", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        highScoreLabel.setFontScale(5); // Increase the font size of the title
        highScoreTable.add(highScoreLabel).colspan(2).padBottom(30); // Add title label and span across 2 columns
        highScoreTable.row(); // Move to the next row

        for (int i = 0; i < highScoreList.size() && i < 10; i++) {
            Player entry = highScoreList.get(i);
            String name = entry.getPlayerName();
            String score = String.valueOf(entry.getHighScore());

            /** Iterating over list and adding a label to the 10 highest scores.*/

            String rank = Integer.toString(i + 1);

            Label rankLabel = new Label(rank + ".", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
            rankLabel.setFontScale(4);
            Label nameLabel = new Label(name, new Label.LabelStyle(new BitmapFont(), Color.WHITE));
            nameLabel.setFontScale(4);
            Label scoreLabel = new Label(score, new Label.LabelStyle(new BitmapFont(), Color.WHITE));
            scoreLabel.setFontScale(4);


            highScoreTable.add(rankLabel).right().padRight(20);
            highScoreTable.add(nameLabel).left();
            highScoreTable.add(scoreLabel).left();
            highScoreTable.row().padTop(10);

        }
    }

    @Override
    protected void handleInput(String name){
    }

    @Override
    public void update(float dt) {
        highScoreList = SnowmanGame.FBI.retrieveHighScores();

    }

    @Override
    public void render(SpriteBatch sb) {
        ScreenUtils.clear(13 / 255f, 0 / 255f, 16 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(backgroundImg, 0, 0, Gdx.graphics.getWidth(), backgroundImg.getHeight() * ((float) Gdx.graphics.getWidth() / backgroundImg.getWidth()));
        sb.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        backgroundImg.dispose();
    }
}
