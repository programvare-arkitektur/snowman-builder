package com.buildasnowman.models;

public class Room {
    private boolean roomReady = false;
    public Player hostPlayer;
    public Player guestPlayer;
    public String roomID = "";

    public Room(String roomID, Player host, Player guest){
        this.roomID = roomID;
        this.hostPlayer = host;
        this.guestPlayer = guest;
    }

    public boolean isRoomReady() {
        return roomReady;
    }

    public void setRoomReady(boolean roomReady) {
        this.roomReady = roomReady;
    }

    public void setHostPlayer(Player host) {
        this.hostPlayer = host;
    }

    public void setGuestPlayer(Player guest) {this.guestPlayer = guest;}

    public void setRoomID(String roomName) {
        this.roomID = roomName;
    }
}
