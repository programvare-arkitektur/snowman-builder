package com.buildasnowman.models;

public class Player {
    private String PlayerID;
    private int highScore;
    private String playerName = "";

    public Player(){
        /** Default constructor required for calls to DataSnapshot.getValue(User.class) */
    }

    public Player(String PlayerID){
        this.PlayerID = PlayerID;
    }

    public String getUID() {
        return PlayerID;
    }

    public int getHighScore() {
        return highScore;

    }

    public void setHighScore(int highScore) {
        if (highScore > this.highScore) {
            this.highScore = highScore;
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;

    }
}
