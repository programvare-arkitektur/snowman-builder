package com.buildasnowman;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import com.buildasnowman.backend.FirebaseInterface;
import com.buildasnowman.controllers.MultiplayerLobbyController;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.views.MenuView;
import com.buildasnowman.controllers.ViewManager;

public class SnowmanGame extends ApplicationAdapter {
    private ViewManager viewManager;
    private SpriteBatch batch;
    public static FirebaseInterface FBI;

    public SnowmanGame(FirebaseInterface firebaseInterface){
        FBI = firebaseInterface;
    }

    @Override
    public void create() {
        FBI.createPlayer();

        batch = new SpriteBatch();
        MultiplayerLobbyController MPcontroller = new MultiplayerLobbyController(this);
        MusicManager musicManager = MusicManager.getInstance();
        musicManager.playBackgroundMusic(); // Start playing background music

        viewManager = new ViewManager(this);
        viewManager.push(new MenuView(viewManager, musicManager, MPcontroller));

    }

    @Override
    public void render() {
        ScreenUtils.clear(1, 0, 0, 1);
        viewManager.update(Gdx.graphics.getDeltaTime() );
        viewManager.render(batch);
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
