package com.buildasnowman.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

/** Manages the background music for the game (singleton)
* This class is responsible for playing and stopping the background music
 */
public class MusicManager {
    private static final MusicManager instance = new MusicManager();
    private final Music backgroundMusic;

    private MusicManager() {
        // Load background music
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("music/music1.mp3"));
        backgroundMusic.setLooping(true); // Loop the background music
    }

    public static MusicManager getInstance() {
        return instance;
    }

    public void playBackgroundMusic() {
        if (!backgroundMusic.isPlaying()) {
            backgroundMusic.play();
        }
    }

    public void stopBackgroundMusic() {
        if (backgroundMusic.isPlaying()) {
            backgroundMusic.stop();
        }
    }

    public boolean isBackgroundMusicPlaying() {
        return backgroundMusic.isPlaying();
    }
}
