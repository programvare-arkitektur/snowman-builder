package com.buildasnowman.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class NavBarController {

    /** Create a navigation bar with a menu button */
    public Table createNavBar(ImageButton menuImageButton) {
        Table navBarTable = new Table();
        navBarTable.setFillParent(true);
        navBarTable.columnDefaults(0).width(Gdx.graphics.getWidth()/6f).height(Gdx.graphics.getHeight()/9f);
        navBarTable.columnDefaults(1).width(Gdx.graphics.getWidth()/6f).height(Gdx.graphics.getHeight()/9f);
        navBarTable.add(menuImageButton).align(Align.left).padRight((Gdx.graphics.getWidth()/6f)*4);
        navBarTable.top();
        return navBarTable;
    }

    /** Create a navigation bar with a menu button and a pause button */
    public Table createNavBar(ImageButton menuImageButton, ImageButton pauseImageButton) {
        Table navBarTable = new Table();
        navBarTable.setFillParent(true);
        navBarTable.columnDefaults(0).width(Gdx.graphics.getWidth()/6f).height(Gdx.graphics.getHeight()/9f);
        navBarTable.columnDefaults(1).width(Gdx.graphics.getWidth()/6f).height(Gdx.graphics.getHeight()/9f);
        navBarTable.add(menuImageButton).align(Align.left).padRight((Gdx.graphics.getWidth()/6f)*4);
        navBarTable.add(pauseImageButton).align(Align.right);
        navBarTable.top();
        return navBarTable;
    }
}
