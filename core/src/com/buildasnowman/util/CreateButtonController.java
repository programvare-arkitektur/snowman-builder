package com.buildasnowman.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * This class is responsible for creating imagebuttons from a provided path
 */
public class CreateButtonController {
    public ImageButton createImageButton(String path) {
        // This class is responsible for creating images
        return new ImageButton(new TextureRegionDrawable(new TextureRegion(new Texture(path))));
    }

}
