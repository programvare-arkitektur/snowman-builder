package com.buildasnowman.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


/**
 * This class is responsible for creating textField from a provided path
 */
public class CreateTextFieldController {

    public TextField createTextField(String fieldText, Float scaleFont, TextureRegionDrawable drawable){
        BitmapFont font = new BitmapFont();
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = font;
        textFieldStyle.fontColor = Color.ORANGE;
        textFieldStyle.font.getData().setScale(scaleFont); // Increase font size by 1.5 times

        if (drawable != null){ // if field should have a background
            textFieldStyle.background = drawable;
        }

        return new TextField(fieldText, textFieldStyle);
    }
}
