package com.buildasnowman.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.buildasnowman.game.GameContext;
import com.buildasnowman.game.gameDraw;
import com.buildasnowman.game.state.PauseState;
import com.buildasnowman.game.state.PlayingState;
import com.buildasnowman.util.MusicManager;
import com.buildasnowman.views.CreateMultiPlayer;
import com.buildasnowman.views.HighScoreView;
import com.buildasnowman.views.JoinMultiPlayer;
import com.buildasnowman.views.MenuView;
import com.buildasnowman.views.SettingsView;
import com.buildasnowman.views.SnowmanGameView;
import com.buildasnowman.views.TutorialView;

public class EventHandler {
    private ViewManager vm;
    private MusicManager musicManager;
    private MultiplayerLobbyController MPController;

    private GameContext game;

    private gameDraw draw;


    public EventHandler( ViewManager vm, MusicManager musicManager, MultiplayerLobbyController MPController) {
        this.vm = vm;
        this.musicManager = musicManager;
        this.MPController = MPController;

    }

    public EventHandler(ViewManager vm, MusicManager musicManager, MultiplayerLobbyController MPController, GameContext game, gameDraw draw){
    this.vm = vm;
        this.musicManager = musicManager;
        this.MPController = MPController;
        this.game = game;
        this.draw = draw;

    }

    public void listener(ImageButton button, String action) {
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput(action);
            }
        });
    }


    protected void handleInput(String name) {
        switch (name) {
            case "playButton":
                vm.set(new SnowmanGameView(vm, musicManager, MPController));
                break;
            case "settingsButton":
                vm.set(new SettingsView(vm, musicManager, MPController));
                break;
            case "createMPButton":
                vm.set(new CreateMultiPlayer(vm, musicManager, MPController));
                break;
            case "joinMPButton":
                vm.set(new JoinMultiPlayer(vm, musicManager, MPController));
                break;
            case "tutorialButton":
                vm.set(new TutorialView(vm, musicManager, MPController));
                break;
            case "highscoreButton":
                vm.set(new HighScoreView(vm, musicManager, MPController));
                break;
            case "menu":
                Gdx.input.setOnscreenKeyboardVisible(false); // Closes the keyboard if left opened
                vm.set(new MenuView(vm, musicManager, MPController));
                break;
            case "joinGame":
                boolean roomReady = MPController.checkRoomStatus(); //checks if the room has registered two users
                if (roomReady) {
                    vm.set(new SnowmanGameView(vm, musicManager, MPController));
                }
                break;
            case "GameMenu":
                game.resetGame();
                draw.resetDraw();
                vm.set(new MenuView(vm, musicManager,MPController));
                break;
            case "pause":
                    if (game.getState() instanceof PlayingState)
                        game.setState(new PauseState(game));
                    else if (game.getState() instanceof PauseState)
                        game.setState(new PlayingState(game));
                    break;
                }

        }
    }

