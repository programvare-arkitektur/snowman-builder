package com.buildasnowman.controllers;

import com.buildasnowman.SnowmanGame;

public class MultiplayerLobbyController {
    SnowmanGame game;

    public MultiplayerLobbyController(SnowmanGame game) {
        this.game = game;
    }

    /**
     * creates a game room if there is not already one opened
     */
    public void createRoom() {
        if (!SnowmanGame.FBI.RoomAlreadyExists()) {
            SnowmanGame.FBI.createRoom();
        }
    }

    public String getRoomCode() {
        return SnowmanGame.FBI.getRID();
    }

    /**
     * Connects to database based on code entered in input field in JoinGameView
     */
    public void connectToRoom(String roomCode) {
        SnowmanGame.FBI.ConnectToRoom(roomCode);
    }

    public boolean checkRoomStatus() {

       return SnowmanGame.FBI.isRoomReady();
    }
    public String getOpponentName(){ return SnowmanGame.FBI.getOpponentName();}

    public int getPlayerScore() { return SnowmanGame.FBI.getMPPlayerScore();}

    public int getOpponentScore() { return SnowmanGame.FBI.getOpponentScore();}

    public int getResult() { return SnowmanGame.FBI.getMPResult();}

}
