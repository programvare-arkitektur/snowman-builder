package com.buildasnowman.controllers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.buildasnowman.SnowmanGame;
import com.buildasnowman.views.View;

import java.util.Stack;

/**
 * A manager for transitions between game views
 */
public class ViewManager {
    private final Stack<View> views;
     public SnowmanGame game;

    public ViewManager(SnowmanGame game){
        this.game = game;
        views = new Stack<>();
    }

    /**
     * Pushes a new view onto the stack.
     */
    public void push(View view) {
        views.push(view);
    }

    /**
     * Replaces the current view with a new view.
     */
    public void set(View view) {
        if (!views.isEmpty()) {
            views.pop().dispose();
        }
        views.push(view);
    }

    /**
     * Updates the view of the application based on the elapsed time.
     * Subclasses should override this method to provide their own update logic.
     */
    public void update(float dt) {
        views.peek().update(dt);
    }

    /**
     * This method is responsible for rendering the current view to the screen.
     * Subclasses should override this method to provide their own rendering logic.
     */
    public void render(SpriteBatch sb) {
        views.peek().render(sb);
    }
}
