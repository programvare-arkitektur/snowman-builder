package com.buildasnowman.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.buildasnowman.game.components.Mappers;
import com.buildasnowman.game.components.PositionComponent;
import com.buildasnowman.game.components.TextureComponent;

import java.util.Locale;

public class gameDraw {
    private static gameDraw instance;
    private final GameContext game;
    Texture backgroundImg, pauseSignImg;
    float backgroundWidth, backgroundHeight, scale, pauseSignWidth, pauseSignHeight;
    int animationScore;
    BitmapFont font;
    Engine engine;

    private gameDraw(GameContext game) {
        this.game = game;
        this.engine = game.engine;

        backgroundImg = new Texture("background/Background.png");
        scale = (float) Gdx.graphics.getWidth() / backgroundImg.getWidth();
        backgroundWidth = backgroundImg.getWidth() * scale;
        backgroundHeight = backgroundImg.getHeight() * scale;

        pauseSignImg = new Texture("gameObjects/PauseSign.png");
        pauseSignWidth = pauseSignImg.getWidth() * scale;
        pauseSignHeight = pauseSignImg.getHeight() * scale;

        font = new BitmapFont();
        animationScore = game.getScore();
    }

    /** Singleton pattern */
    public static gameDraw getInstance(GameContext game) {
        if (instance == null) {
            instance = new gameDraw(game);
        }
        return instance;
    }

    public void draw(SpriteBatch sb) {
        font.getData().setScale(3f);    // Set the scale of the font

        drawBackground(sb);             // Draw the background
        drawEntities(sb);               // Draw entities in ECS
        drawScore(sb);                  // Draw the score
        drawTime(sb);                   // Draw the time
    }

    private void drawBackground(SpriteBatch sb) {
        if (game.getScore() > animationScore) animationScore += 3;
        if (game.getScore() < animationScore) animationScore --;

        // Background is drawn, uses the score to set the start of the image further down to emulate scrolling when the score is increased.
        sb.draw(backgroundImg, 0, (-1 * animationScore), backgroundWidth, backgroundHeight);
    }

    private void drawScore(SpriteBatch sb) {
        font.draw(sb, "Score " + game.getScore() + " ", 20, Gdx.graphics.getHeight() - 20);
    }

    private void drawTime(SpriteBatch sb) {
        // Convert timer-time to mm:ss format
        int minutes = (game.time / 60);
        int seconds = (game.time % 60);

        // Format mm:ss
        String timeString = String.format(Locale.ENGLISH, "%02d:%02d", minutes, seconds);

        font.draw(sb, timeString, (float) Gdx.graphics.getWidth() / 2, (float) Gdx.graphics.getHeight() - 20);
    }

    private void drawEntities(SpriteBatch sb) {
        for (Entity entity : engine.getEntitiesFor(Family.all(TextureComponent.class, PositionComponent.class).get())) {
            PositionComponent position = Mappers.position.get(entity);
            TextureComponent texture = Mappers.texture.get(entity);

            texture.sprite.setPosition(position.x, position.y);
            texture.sprite.draw(sb);
        }
    }

    public void pauseDraw(SpriteBatch sb){
        sb.draw(pauseSignImg, (float) Gdx.graphics.getWidth() /2 - pauseSignWidth/2, (float) Gdx.graphics.getHeight() /2 - pauseSignHeight/2, pauseSignWidth, pauseSignHeight);
    }

    public void resetDraw() {instance = null;}

    public void dispose() {
        backgroundImg.dispose();
    }

}
