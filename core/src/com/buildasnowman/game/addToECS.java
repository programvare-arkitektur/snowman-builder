package com.buildasnowman.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.buildasnowman.game.components.CollisionComponent;
import com.buildasnowman.game.components.FallingComponent;
import com.buildasnowman.game.components.PositionComponent;
import com.buildasnowman.game.components.ScoreComponent;
import com.buildasnowman.game.components.TextureComponent;

public class addToECS {

    Engine engine;

    public addToECS(Engine engine){
        this.engine = engine;
    }

    // Add player entity to ECS
    public Entity createSnowmanEntity(Texture snowmanImg, int objectSize){
        Entity snowman = new Entity();
        snowman.add(new PositionComponent(1,objectSize/-2));
        snowman.add(new CollisionComponent(0, 0, objectSize*2, objectSize*6));
        snowman.add(new TextureComponent(snowmanImg, objectSize*2, objectSize*6));
        snowman.add(new ScoreComponent(0));
        return snowman;
    }

    // Add falling object to ECS
    public void addFallingObjectToECS(int amount, int speed, int score, int size, Texture img) {
        // Add falling object to ECS
        for (int i = 0; i < amount; i++) {
            int positionX = getScreenDistributionPosition(i, amount, size);

            Entity entity = new Entity();
            entity.add(new ScoreComponent(score));
            entity.add(new CollisionComponent(positionX, Gdx.graphics.getHeight(), size, size));
            entity.add(new TextureComponent(img, size, size));

            entity.add(new FallingComponent(speed));

            // equally distributes the positions of the fire entities across the screen
            entity.add(new PositionComponent(positionX));

            engine.addEntity(entity);
        }
    }

    /** Calculates the position of the fire or snowball entities across the screen based on the amount of entities */
    public int getScreenDistributionPosition(int position, int amount, int size){
        int width = Gdx.graphics.getWidth();
        int segment = width / (amount + 1); // Needs one more segment than there are objects so that the last object is not placed at the rightmost part of the screen.
        int placement = segment * (position + 1); // Adjusts for the fact that the first is indexed as 0
        placement -= size/2; // Adjusts the placement to originate from the center of the image
        return placement;
    }

    /** Add ground entity to ECS */
    public void addGroundToECS() {
        Entity ground = new Entity();
        ground.add(new CollisionComponent(0, 0, Gdx.graphics.getWidth(), 1));
        ground.add(new PositionComponent(0, 0));

        engine.addEntity(ground);
    }

}
