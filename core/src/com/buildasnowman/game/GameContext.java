package com.buildasnowman.game;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.buildasnowman.SnowmanGame;
import com.buildasnowman.game.state.State;
import com.buildasnowman.game.systems.CollisionSystem;
import com.buildasnowman.game.systems.FallingSystem;

public class GameContext {
    private static GameContext instance;
    public State currentState;
    public Engine engine;
    public Entity snowman;
    public int time, animationScore;
    private int score;

    private GameContext() {
        // Set the initial state
        engine = new Engine();

        animationScore = 0;
        Texture snowmanImg = new Texture("gameObjects/snowman/Snowman.png");
        Texture fireImg = new Texture("gameObjects/Fire.png");
        Texture snowBallImg = new Texture("gameObjects/Snowball.png");

        // Set the size of the objects based on the screen size
        int objectSize = Gdx.graphics.getHeight() / 24;

        com.buildasnowman.game.addToECS addToECS = new addToECS(engine);
        // Initialize fire and snowball entities
        addToECS.addFallingObjectToECS(4, objectSize /10, -25, objectSize, fireImg); // Less fire than snowballs to make the game easier
        addToECS.addFallingObjectToECS(6, objectSize /10, 100, objectSize, snowBallImg); // 6 snowballs is the highest amount before the hitbox of the snowman collides with multiple snowballs at once

        // Add ground to ECS
        addToECS.addGroundToECS();

        // Add snowman to ECS
        snowman = addToECS.createSnowmanEntity(snowmanImg, objectSize);
        engine.addEntity(snowman);

        // Sets the duration of the game in seconds
        time = 70;

        // Add systems to the engine
        engine.addSystem(new FallingSystem());
        engine.addSystem(new CollisionSystem());
    }

    public static GameContext getInstance() {
        if (instance == null) {
            instance = new GameContext();
        }
        return instance;
    }

    public void resetGame() {
        instance = null;
    }

    public void setState(State state) {
        currentState = state;
    }

    public State getState() {
        return currentState;
    }

    public void handleGame(float deltaTime, SpriteBatch sb) {
        currentState.handleGame(deltaTime, sb);
    }
    public void dispose() {
        currentState.dispose();
    }

    //Handling score across states
    public int getScore() {
        return score;
    }

    public void setScore(int points) {
        score = points;
        SnowmanGame.FBI.SetPlayerScoreInDb("highScore", score);
    }
}
