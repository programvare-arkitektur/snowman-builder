package com.buildasnowman.game.state;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.buildasnowman.game.GameContext;
import com.buildasnowman.game.gameDraw;

public class PauseState implements State {
    gameDraw draw;

    public PauseState(GameContext game) {
        draw = gameDraw.getInstance(game);
    }

    @Override
    public void handleGame(float deltaTime, SpriteBatch sb) {
        draw.pauseDraw(sb);
    }
    public void dispose(){
    }
}
