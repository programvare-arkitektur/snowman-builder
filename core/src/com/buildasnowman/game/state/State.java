package com.buildasnowman.game.state;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface State {
    void handleGame(float deltaTime, SpriteBatch sb);
    void dispose();
}


