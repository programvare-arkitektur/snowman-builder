package com.buildasnowman.game.state;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.buildasnowman.game.GameContext;
import com.buildasnowman.game.components.Mappers;
import com.buildasnowman.game.components.PositionComponent;
import com.buildasnowman.game.components.ScoreComponent;
import com.buildasnowman.game.components.TextureComponent;

public class PlayingState implements State {
	private final GameContext game;
	float timerAccumulator, countdownInterval;
	Engine engine;
	public PlayingState(GameContext game) {
		this.game = game;
		this.engine = game.engine;
		timerAccumulator = 0; // Accumulates deltaTime until it reaches one second
		countdownInterval = 1f; // Countdown interval in seconds (1 second)
	}

	/** Retrieve the score from the snowman entity to the game context */
	public void retrieveScore() {
		game.setScore(game.snowman.getComponent(ScoreComponent.class).getScore());
	}

	@Override
	public void handleGame(float deltaTime, SpriteBatch sb) {
		engine.update(deltaTime); // Runs the systems in the ECS
		retrieveScore(); // Retrieves the score from the snowman entity and applies it to the GameContext score
		moveSnowman(); // Moves the Snowman Entity
		timer(); // Counts down the playing time
	}

	private void moveSnowman() {
		// Get the x-coordinate of the mouse cursor
		float mouseX = Gdx.input.getX();

		// Adjust the position of the snowman rectangle based on the mouse cursor position
		PositionComponent snowmanPosition = Mappers.position.get(game.snowman);
		TextureComponent snowmanSize = Mappers.texture.get(game.snowman);
		snowmanPosition.x = (int) (mouseX - snowmanSize.width / 2);

		// Ensure the rectangle stays within the screen bounds
		if (snowmanPosition.x < 0) {
			snowmanPosition.x = 0;
		}
		if (snowmanPosition.x + snowmanSize.width > Gdx.graphics.getWidth()) {
			snowmanPosition.x = Gdx.graphics.getWidth() - snowmanSize.width;
		}
	}

	private void timer() {
		timerAccumulator += Gdx.graphics.getDeltaTime(); // Accumulate deltaTime

		// Check if one second has passed
		if (timerAccumulator >= countdownInterval) {
			// Decrement the timer by one second
			game.time -= 1;
			timerAccumulator -= countdownInterval; // Reset accumulator
		}

		if (game.time <= 0) {
			// Timer has reached zero or negative value
			game.time = 0; // Set timer to zero to prevent negative values
			game.setState(new TimeUpState(game));
		}
	}

	/** This method is called when the application is shutting down. It's used for cleaning up resources to avoid memory leaks.*/
	public void dispose() {
	}

}
