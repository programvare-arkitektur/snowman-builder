package com.buildasnowman.game.state;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.buildasnowman.game.GameContext;

public class TimeUpState implements State {
    GameContext game;
    public TimeUpState(GameContext game) {
        this.game = game;
    }
    @Override
    public void handleGame(float deltaTime, SpriteBatch sb) {}
    public void dispose(){

    }
}
