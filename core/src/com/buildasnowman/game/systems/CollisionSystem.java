package com.buildasnowman.game.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.buildasnowman.game.components.CollisionComponent;
import com.buildasnowman.game.components.FallingComponent;
import com.buildasnowman.game.components.Mappers;
import com.buildasnowman.game.components.PositionComponent;
import com.buildasnowman.game.components.ScoreComponent;

public class CollisionSystem extends EntitySystem {
    private ImmutableArray<Entity> collidableObjects;
    private ImmutableArray<Entity> collidableObjectsNonFalling;

    public void addedToEngine(Engine engine) {
        collidableObjects = engine.getEntitiesFor(Family.all(CollisionComponent.class, FallingComponent.class).get()); //Fire and snowballs
        collidableObjectsNonFalling = engine.getEntitiesFor(Family.all(CollisionComponent.class).exclude(FallingComponent.class).get()); //Snowman and ground
    }

    public void update(float deltaTime){ // Check for collisions between part A and B where A is either Snowman or ground, and B is either fire or snowball.
        for (Entity entityA : collidableObjectsNonFalling ) { // Check for collisions based on the non-moveable objects
            PositionComponent positionA = Mappers.position.get(entityA);
            CollisionComponent collisionA = Mappers.collision.get(entityA);
            ScoreComponent scoreA = Mappers.score.get(entityA);

            //Updates the position of the hitbox to the actual position of the object
            collisionA.updatePosition(positionA.x, positionA.y);

            for (Entity entityB : collidableObjects) {
                CollisionComponent collisionB = Mappers.collision.get(entityB);
                PositionComponent positionB = Mappers.position.get(entityB);
                ScoreComponent scoreB = Mappers.score.get(entityB);

                collisionB.updatePosition(positionB.x, positionB.y);

                if (collisionA.rectangle.overlaps(collisionB.rectangle)) {
                    // If the snowman collides with a fire or snowball, the object is reset to a starting position above the screen.
                    positionB.y = (int) (Gdx.graphics.getHeight() + Math.random()*Gdx.graphics.getHeight());

                    // Adds the score-value of the fire or snowball to the total score of the snowman (ground does not have score-component)
                    if (entityA.getComponent(ScoreComponent.class) != null && entityB.getComponent(ScoreComponent.class) != null) {
                        scoreA.addScore(scoreB.getScore());

                        // If the score of the snowman is below 0, the score is reset to 0, to avoid negative scores
                        if (scoreA.getScore() < 0) {
                            scoreA.resetScore();
                        }
                    }
                }
            }
        }
    }
}
