package com.buildasnowman.game.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.buildasnowman.game.components.FallingComponent;
import com.buildasnowman.game.components.Mappers;
import com.buildasnowman.game.components.PositionComponent;

public class FallingSystem extends EntitySystem
{
    private ImmutableArray<Entity> fallingObjects;

    public void addedToEngine(Engine engine) {
        fallingObjects = engine.getEntitiesFor(Family.all(FallingComponent.class).get());
    }

    public void update(float deltaTime){
        for (Entity entity : fallingObjects ) {
            PositionComponent position = Mappers.position.get(entity);
            FallingComponent fallingSpeed = Mappers.fallingSpeed.get(entity);

            float screenHeight = Gdx.graphics.getHeight() ;
            float speedMultiplier = 1f;

            if (position.y < screenHeight) {
                speedMultiplier = 1f + ((screenHeight - position.y) / screenHeight) * 6f;
            }

            position.y = (int) (position.y - fallingSpeed.speed * speedMultiplier);
        }
    }
}

