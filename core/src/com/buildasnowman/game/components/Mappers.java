package com.buildasnowman.game.components;

import com.badlogic.ashley.core.ComponentMapper;

/** Denne klassen blir brukt for å hente ut verdien til entitiene som er lagt i componentene
* PositionComponent pos = Mappers.position.get(entity); Der entity er de du definerer selv.
*/
 public class Mappers {
    public static final ComponentMapper<PositionComponent> position = ComponentMapper.getFor(PositionComponent.class);
    public static final ComponentMapper<FallingComponent> fallingSpeed = ComponentMapper.getFor(FallingComponent.class);
    public static final ComponentMapper<CollisionComponent> collision = ComponentMapper.getFor(CollisionComponent.class);
    public static final ComponentMapper<TextureComponent> texture = ComponentMapper.getFor(TextureComponent.class);
    public static final ComponentMapper<ScoreComponent> score = ComponentMapper.getFor(ScoreComponent.class);
}
