package com.buildasnowman.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.Gdx;

public class PositionComponent implements Component {
    public int x;
    public int y;

    /** this constructor is used for falling objects that need a random y positon above the screen to start */
    public PositionComponent(int x) {
        this.x = x;
        this.y = (int) (Gdx.graphics.getHeight() + Math.random()*Gdx.graphics.getHeight());
    }

    /** this constructor is used for an object that needs a specific x and y position to start */
    public PositionComponent(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
