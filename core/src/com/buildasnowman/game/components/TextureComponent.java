package com.buildasnowman.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class TextureComponent implements Component {
    public Sprite sprite;
    public int width;
    public int height;

    public TextureComponent(Texture texture, float width, float height) {
        this.sprite=new Sprite(texture);
        this.sprite.setSize(width,height);
        this.width = (int) width;
        this.height = (int) height;
    }
}
