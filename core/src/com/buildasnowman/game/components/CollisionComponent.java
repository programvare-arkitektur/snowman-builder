package com.buildasnowman.game.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Rectangle;

public class CollisionComponent implements Component {
    public Rectangle rectangle;

    public CollisionComponent(int posX, int posY, int width, int height) {
        this.rectangle = new Rectangle(posX, posY, width, height);
    }
    public void updatePosition(int posX, int posY) {
        this.rectangle.setPosition(posX, posY);
    }
}
