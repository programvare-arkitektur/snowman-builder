package com.buildasnowman.game.components;

import com.badlogic.ashley.core.Component;

public class FallingComponent implements Component {
    public float speed;
    public FallingComponent(float speed) {
        this.speed = speed;
    }
}
