package com.buildasnowman.game.components;

import com.badlogic.ashley.core.Component;

public class ScoreComponent implements Component {

    private int score;

    public ScoreComponent(int amount) {
        score = amount;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public void resetScore() {
        score = 0;
    }

}
